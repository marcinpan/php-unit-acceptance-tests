<?php
    error_reporting(-1);
    ini_set("display_errors", "On");

    require_once ("../vendor/autoload.php");
    session_start();

    Path::init(__DIR__. "/../");
    Env::init();
    Log::init();

    $app = new App();
    $req = new Request();

    $app->handle($req);
