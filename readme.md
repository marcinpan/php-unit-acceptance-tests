# What to do?

1) Copy this project to your repository

2) Setup test environment using below instruction

3) Fix unit test by implementing RingBuffer class

4) Fix acceptance test by adding appropriate controllers/views/models etc.

5) Commit all your changes to **app/** directory under **lesson_07/**

When grading projects, directory **app/** from your repo will be copied and all test will be executed on it.
Any other changes will be ignored! Make sure that you change only that directory!


#### Points (0-3)

There are two basic points to get if all test are green.
If there is single test case failing you can still get one point.

There is also one bonus point for exceptionally clean/pretty code!

#### Deadline

Deadline to submit all your changes: **02.12.2016** (including that day).  

-----

# Setup procedure

Nginx should be configured to run site under **http://localhost:8007/** !

       server {
         
            listen 8007 default_server;
            listen [::]:8007 default_server;
            
            # Something other here ...
            
            root /home/student/PhpstormProjects/lesson_07/public;
            
            # Something other here ...
            
            location / {
            
                 try_files $uri $uri/ /index.php;
            }
       
            # Something other here ...
       }

Remember to set **port**, **root** and **try_files** !

Enter **lesson_07** directory and execute:

      composer install
      mkdir storage
      chmod 0777 storage
      cp .env.example .env
      vim .env
      
      # DB_CONNECTION=mysql
      # DB_DATABASE=test
      # DB_USERNAME=test
      # DB_PASSWORD=test123

      alias codecept='./vendor/bin/codecept'
      
      codecept run

Everything should be green!

If you want to use Selenium with Google Chrome:

      cd ~
      mkdir selenium
      cd selenium
      
      # Download jar file from http://www.seleniumhq.org/download/
      mv ~/Pobrane/selenium-server-standalone-3.0.1.jar .

      # Check your brovser version
      google-chrome-stable --version
      
      # Download and extract chromedriver_linux64.zip from: http://chromedriver.storage.googleapis.com/index.html?path=2.22/
      # Driver must be comaptible with your browser version!
      # My version was: 52.0.2743.116 and driver 2.22 works - but 2.23 does not because Chrome was too new.
      # Compatibility can be checked in notes.txt file in directory with given version.
      
      mv ~/Pobrane/chromedriver_linux64.zip .
      unzip chromedriver_linux64.zip 
      rm chromedriver_linux64.zip 
      
      # You should have two files: chromedriver, selenium-server-standalone-3.0.1.jar
      
      java -jar selenium-server-standalone-3.0.1.jar
      # Let it run in background
      
      # In other console enter the lesson_07 directory and:
      
      vim tests/acceptance.suite.yml

      # Replace modules by commented-out section
      
      codecept run


Hint: if you want to use production (student) database:

      vim .env
      
      # DB_CONNECTION=mysql
      # DB_DATABASE=student
      # DB_USERNAME=student
      # DB_PASSWORD=student123
      
      # Import data
      mysql -u student student -p < database/dump.sql
