<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('check homepage');
//
$I->amOnPage("/");
$I->seeInTitle("Home Page");
$I->see("Welcome to Codeception exercise!");

$I->amGoingTo("create a book");

$I->click("Books");
$I->seeInCurrentUrl("/books");

$I->click("New book");
$I->seeInCurrentUrl("/books/create");

$I->see("New book", "h1");

$title = "Harry Potter";
$description = "Something here...";

$I->fillField("title", $title);
$I->fillField("description", $description);

$I->dontSeeInDatabase("books", ["title" => $title]);

$I->click("Add");

$I->seeInDatabase("books", ["title" => $title]);

$id = $I->grabFromDatabase("books", "id", ["title" => $title]);

$I->seeInCurrentUrl("/books/show/$id");

$I->see($title, "h1");
$I->see($description, "p");

