<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('create user when all fields are empty');

$I->amOnPage("/users/create");

$I->click("Create");

$I->seeCurrentUrlEquals("/users/create");

$I->lookForwardTo("see validation errors");

$I->see("The name filed cannot be empty", "li.error");
$I->see("The surname filed cannot be empty", "li.error");
$I->see("The email filed cannot be empty", "li.error");
$I->see("The password filed cannot be empty", "li.error");
$I->see("The password confirmation filed cannot be empty", "li.error");

$I->amGoingTo("check validation error when password confirmation is invalid");

$I->fillField("name", "Name");
$I->fillField("surname", "Surname");
$I->fillField("email", "foo@bar.com");
$I->fillField("password", "vey_long_and_complex_password");
$I->fillField("password_confirmation", "vey_long_and_complex_password_confirmation_with_error");

$I->click("Create");
$I->seeCurrentUrlEquals("/users/create");

$I->see("The password confirmation filed does not match the password field", "li.error");
