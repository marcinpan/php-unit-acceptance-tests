<?php
$I = new AcceptanceTester($scenario);

$I->amOnPage("/");

$I->wantTo('open users page');

$I->click("Users");

$I->seeInCurrentUrl("/users");
$I->seeInTitle("Users");

$I->see("No users found!", "p.error");

$I->amGoingTo("create new user");

$I->click("New user");

$I->seeInCurrentUrl("/users/create");
$I->seeInTitle("New user");

$I->see("New user", "h1");

$name = "John";
$surname = "Doe";
$email = "john.doe@example.com";
$password = "john123";
$password_confirmation = "john123";

$I->fillField("name", $name);
$I->fillField("surname", $surname);
$I->fillField("email", $email);
$I->fillField("password", $password);
$I->fillField("password_confirmation", $password);

$I->dontSeeInDatabase("users", ["email" => $email]);

$I->click("Create");

$I->seeInDatabase("users", [
    "name" => $name,
    "surname" => $surname,
    "email" => $email,
    "confirmed" => false
]);

$I->amGoingTo("make sure that password was securely hashed");

$hash = $I->grabFromDatabase("users", "password", ["email" => $email]);
$I->assertTrue(password_verify($password, $hash));

$I->amGoingTo("check that email confirmation token was generated");
$I->comment("token should be randomly generated hexadecimal value");

$token = $I->grabFromDatabase("users", "token", ["email" => $email]);
$I->assertNotEmpty($token);
$I->assertEquals(32, strlen($token));
$I->assertTrue(ctype_xdigit($token));

$I->amGoingTo("check redirection to user page after creation");

$id = $I->grabFromDatabase("users", "id", ["email" => $email]);
$I->seeCurrentUrlEquals("/users/show/$id");
$I->seeInTitle("$name $surname");