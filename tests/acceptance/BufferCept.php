<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('test ring buffer directly from browser');

$I->amOnPage("/");
$I->click("Buffer");

$I->seeCurrentUrlEquals("/buffer");
$I->seeInTitle("Buffer");

$I->see("Current size: 0", "h2");
$I->see("Current capacity: 3", "h3");

$I->seeInField("input#input_0:disabled", "");
$I->seeInField("input#input_1:disabled", "");
$I->seeInField("input#input_2:disabled", "");

$I->amGoingTo("push new value several times");
$I->lookForwardTo("see that values from tail are discarded");

$I->fillField("value", "Foo");
$I->click("Push");

$I->seeInCurrentUrl("/buffer");

$I->see("Current size: 1", "h2");
$I->see("Current capacity: 3", "h3");

$I->seeInField("input#input_0:disabled", "Foo");
$I->seeInField("input#input_1:disabled", "");
$I->seeInField("input#input_2:disabled", "");

$I->fillField("value", "Bar");
$I->click("Push");

$I->fillField("value", "Baz");
$I->click("Push");

$I->seeInCurrentUrl("/buffer");

$I->see("Current size: 3", "h2");
$I->see("Current capacity: 3", "h3");

$I->seeInField("input#input_0:disabled", "Foo");
$I->seeInField("input#input_1:disabled", "Bar");
$I->seeInField("input#input_2:disabled", "Baz");


$I->fillField("value", "Daz");
$I->click("Push");

$I->seeInCurrentUrl("/buffer");

$I->see("Current size: 3", "h2");
$I->see("Current capacity: 3", "h3");

$I->seeInField("input#input_0:disabled", "Bar");
$I->seeInField("input#input_1:disabled", "Baz");
$I->seeInField("input#input_2:disabled", "Daz");

$I->amGoingTo("pop values from buffer");

$I->click("Pop");

$I->seeInCurrentUrl("/buffer");
$I->see("Current size: 2", "h2");
$I->see("Current capacity: 3", "h3");

$I->seeInField("input#input_0:disabled", "Baz");
$I->seeInField("input#input_1:disabled", "Daz");
$I->seeInField("input#input_2:disabled", "");

$I->click("Pop");
$I->click("Pop");

$I->seeInCurrentUrl("/buffer");
$I->see("Current size: 0", "h2");
$I->see("Current capacity: 3", "h3");

$I->seeInField("input#input_0:disabled", "");
$I->seeInField("input#input_1:disabled", "");
$I->seeInField("input#input_2:disabled", "");
