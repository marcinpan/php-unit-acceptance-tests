<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('see error when user email is not confirmed');

$name = "Unconfirmed";
$surname = "User";
$email = "dummy@example.com";
$password = password_hash("foo", PASSWORD_DEFAULT);
$token = md5(uniqid(rand(), true));

$id = $I->haveInDatabase("users", [
    "name" => $name,
    "surname" => $surname,
    "email" => $email,
    "password" => $password,
    "confirmed" => 0,
    "token" => $token
]);

$I->amOnPage("/users/show/$id");
$I->see("Please confirm your email by clicking link sent to $email", "p.error");

$I->wantTo("see an error when token is invalid");

$I->amOnPage("/users/confirm/ThisIsAnInvalidEmailConfirmationToken");

$I->seeCurrentUrlEquals("/");
$I->see("Provided token is invalid!", "li.error");

$I->wantTo("check that error disappears after page refresh");
$I->amOnPage("/");
$I->dontSee("Provided token is invalid!");

$I->amGoingTo("confirm user using valid token");
$I->amOnPage("/users/confirm/$token");
$I->seeCurrentUrlEquals("/");
$I->dontSee("Provided token is invalid!");
$I->see("Email successfully confirmed!", "li.info");

$I->seeInDatabase("users", [
    "email" => $email,
    "confirmed" => 1,
    "token" => null
]);

$I->wantTo("check that info disappears after page refresh");
$I->amOnPage("/");
$I->dontSee("Email successfully confirmed!");

$I->wantTo('see no error on user page because email is now confirmed');

$I->amOnPage("/users/show/$id");
$I->dontSee("Please confirm your email by clicking link sent to $email");


