<?php

namespace Model;

use Database;
use PDO;

class Book {

    public $id;
    public $title;
    public $description;

    public static function all()
    {
        $handle = Database::handle();
        $statement = $handle->query("SELECT * FROM books");
        return $statement->fetchAll(PDO::FETCH_CLASS, Book::class);
    }

    public static function find($id)
    {
        $handle = Database::handle();
        $statement = $handle->prepare("SELECT * FROM books WHERE id=:id");
        $statement->bindParam(":id", $id);
        $statement->execute();
        return $statement->fetchObject(static::class);
    }

    public function save()
    {
        $handle = Database::handle();
        $statement = $handle->prepare("INSERT INTO `books`(`id`, `title`, `description`) VALUES (NULL, ?, ?)");
        $statement->execute([$this->title, $this->description]);
        $this->id = $handle->lastInsertId();
    }
}