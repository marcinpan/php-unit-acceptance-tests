<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 29.11.16
 * Time: 02:26
 */

namespace Model;

use Database;
use PDO;

class RingBuffer
{
    public static function create($range)
    {
        $buffer = new \Container\RingBuffer($range);
        return $buffer;
    }
}