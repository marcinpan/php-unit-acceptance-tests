<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 30.11.16
 * Time: 22:56
 */

namespace Model;

use Database;
use PDO;

class User
{
    public $id;
    public $name;
    public $surname;
    public $email;
    public $password;
    public $confirmed = 0;
    public $token;


    public static function all() {
        $handle = Database::handle();
        $statement = $handle->query("SELECT * FROM users");
        return $statement->fetchAll(PDO::FETCH_CLASS, User::class);
    }
    public static function find($id)
    {
        $handle = Database::handle();
        $statement = $handle->prepare("SELECT * FROM users WHERE id=:id");
        $statement->bindParam(":id", $id);
        $statement->execute();
        return $statement->fetchObject(static::class);
    }
    public static function findByToken($token)
    {
        $handle = Database::handle();
        $statement = $handle->prepare("SELECT * FROM users WHERE token=:token");
        $statement->bindParam(":token", $token);
        $statement->execute();
        return $statement->fetchObject(static::class);
    }
    public function save()
    {
        $handle = Database::handle();
        $password = password_hash($this->password, PASSWORD_BCRYPT);
        $token = bin2hex(openssl_random_pseudo_bytes(16));
        $statement = $handle->prepare("INSERT INTO `users`(`id`, `name`, `surname`, `email`, `password`, `confirmed`, `token`) VALUES (NULL, ?, ?, ?, ?, false, ?)");
        $statement->execute([$this->name, $this->surname, $this->email, $password, $token]);
        $this->id = $handle->lastInsertId();
    }
    public function update()
    {
        $handle = Database::handle();
        $statement = $handle->prepare("UPDATE `users` SET `confirmed`=1, `token`=NULL WHERE id=".$this->id);
        $statement->execute();
    }
}