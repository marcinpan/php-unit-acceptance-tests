<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Log {

    private static $log;

    public static function init() {
        static::$log = new Logger('app');
        $file = Path::storage() . "/app.log";
        $level = Logger::DEBUG;
        static::$log->pushHandler(new StreamHandler($file, $level));
    }

    public static function debug(string $what) {
        static::$log->debug($what);
    }

    public static function info(string $what) {
        static::$log->info($what);
    }

    public static function error(string $what) {
        static::$log->error($what);
    }
}