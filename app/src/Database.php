<?php

class Database
{
    private static $handle;

    /**
     * @return PDO
     */
    public static function handle() {

        if (static::$handle) {
            return static::$handle;
        }

        $dsn = static::dsn();
        $user = static::user();
        $password = static::password();

        Log::debug("Connecting dsn: '$dsn', user: '$user', password: '$password'");

        static::$handle = new PDO($dsn, $user, $password);
        static::$handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return static::$handle;
    }

    public static function dsn() {

        $connection = Env::get("DB_CONNECTION");

        if ($connection == "mysql") {

            $db = Env::get("DB_DATABASE");

            return "mysql:dbname=$db";
        }

        return "";
    }

    public static function user() {
        return Env::get("DB_USERNAME");
    }

    public static function password() {
        return Env::get("DB_PASSWORD");
    }
}
