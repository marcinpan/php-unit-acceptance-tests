<?php

use Controller\HomeController;
use Controller\ErrorController;
use Controller\TableController;
use Controller\BookController;
use Controller\BufferController;
use Controller\UsersController;

class App
{
    public function handle(Request $req) {

        $page = $req->page();

        Log::info("Handle page: '$page'");

        if ($page == "books") {
            $controller = new BookController();
        } elseif ($page == "buffer") {
            $controller = new BufferController();
        } elseif ($page == "users") {
            $controller = new UsersController();
        } elseif ($page == "") {
            $controller = new HomeController();
        } else {
            Log::error("Unknown page: '$page'");

            $controller = new ErrorController();
        }

        $controller->handle($req);
    }
}