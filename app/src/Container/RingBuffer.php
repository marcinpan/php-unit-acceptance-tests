<?php

namespace Container;


use GuzzleHttp\Promise\Tests\Thing1;

class RingBuffer
{
    protected $capacity;
    protected $size = 0;
    protected $buffer;

    public function __construct($capacity) {
        $this->capacity = intval($capacity);
        $this->buffer = array();
    }

    public function empty() { return boolval($this->size() == 0); }

    public function capacity() { return $this->capacity; }

    public function size() { return $this->size; }

    public function full() { return boolval($this->size == $this->capacity); }

    public function push($val) {
        if( $this->full() ) $this->pop();
        $this->buffer[$this->size++] = $val;
    }

    public function pop() {
        if(!$this->empty()) $this->size--;
        return array_shift($this->buffer);
    }

    public function tail() {
        if(!$this->empty()) return $this->buffer[0];
        return NULL;
    }

    public function head() {
        if(!$this->empty()) return $this->buffer[$this->size() - 1];
        return NULL;
    }

    public function at($i) {
        if(!array_key_exists($i, $this->buffer)) return FALSE;
        return $this->buffer[$i];
    }
}