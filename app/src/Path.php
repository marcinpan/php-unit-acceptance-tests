<?php

class Path
{
    private static $root;

    public static function init(string $root) {
        static::$root = $root;
    }

    public static function root() {
        return static::$root;
    }

    public static function app() {
        return static::root() . "/app/";
    }

    public static function storage() {
        return static::root() . "/storage/";
    }

    public static function view() {
        return static::app() . "/view/";
    }

    public static function src() {
        return static::app() . "/src/";
    }
}