<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 30.11.16
 * Time: 22:45
 */

namespace Controller;

use Model\User;

class UsersController extends Controller
{
    function index()
    {
        $users = User::all();
        return ["/users/index.php", ['users' => $users]];
    }
    function show($id)
    {
        $user = User::find($id);
        return ["/users/show.php", ['user' => $user]];
    }
    function create()
    {
        if(isset($_POST['Button'])) {
            $errorArray = [];
            if (empty($_POST['name'])) {
                $nameError = "The name filed cannot be empty";
                $errorArray[] = $nameError;
            }
            if (empty($_POST['surname'])) {
                $surnameError = "The surname filed cannot be empty";
                $errorArray[] = $surnameError;
            }
            if (empty($_POST['email'])) {
                $emailError = "The email filed cannot be empty";
                $errorArray[] = $emailError;
            }
            if (empty($_POST['password'])) {
                $passwordError = "The password filed cannot be empty";
                $errorArray[] = $passwordError;
            }
            if (empty($_POST['password_confirmation'])) {
                $passwordConfirmationError = "The password confirmation filed cannot be empty";
                $errorArray[] = $passwordConfirmationError;
            }
            $correctPassword = true;
            if($_POST['password'] != $_POST['password_confirmation']){
                $correctPassword = false;
            }

            if (!empty($errorArray) || !$correctPassword) {
                return ["/users/create.php", ['errorArray' => $errorArray, 'correctPassword' => $correctPassword]];
            } else {
                $user = new User();
                $user->name = $_POST['name'];
                $user->surname = $_POST['surname'];
                $user->email = $_POST['email'];
                $user->password = $_POST['password'];
                $user->save();

                header('Location: /users/show/' . $user->id);
            }
        }
        else {
            return ["/users/create.php"];
        }
    }
    function confirm($token)
    {
        $user = User::findByToken($token);

        if (!$user) {
            $_SESSION['token'] = 'invalid';
            header("Location: /");
        } else {
            $_SESSION['token'] = 'valid';
            $user->update();
            header("Location: /");
        }
    }
}