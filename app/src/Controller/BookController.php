<?php

namespace Controller;

use Model\Book;

class BookController extends Controller
{
    function index()
    {
        $books = Book::all();

        return ["/book/index.php", ['books' => $books]];
    }

    function create() {

        return ["/book/create.php"];
    }

    function store() {

        $book = new Book();
        $book->title = $_POST["title"];
        $book->description = $_POST["description"];
        $book->save();

        header('Location: /books/show/' . $book->id);
    }

    function show($id) {

        $book = Book::find($id);

        return ["/book/show.php", ["book" => $book]];
    }
}
