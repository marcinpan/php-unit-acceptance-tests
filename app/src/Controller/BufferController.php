<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 29.11.16
 * Time: 01:06
 */

namespace Controller;

use Model\RingBuffer;

class BufferController extends Controller
{
    function index()
    {
        $ringBuffer = RingBuffer::create(3);
        return ["/buffer/index.php", ["buffer" => $ringBuffer]];
    }

    function push()
    {
        $_SESSION['value'] = $_POST['value'];
        $_SESSION['push'] = true;
        header('Location: /buffer');
    }
    function pop()
    {
        $_SESSION['pop'] = true;
        header('Location: /buffer');
    }


}