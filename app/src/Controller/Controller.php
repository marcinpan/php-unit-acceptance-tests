<?php

namespace Controller;

use Request;
use Path;

abstract class Controller
{
    function handle(Request $req)
    {
        $parameters = $req->parameters();
        $method = array_shift($parameters);

        if ($method == null || $method == "") {
            $result = $this->index();
        } else {
            // Not index? Call appropriate method from controller class
            $result = call_user_func_array([$this, $method], $parameters);
        }

        $root = Path::view();
        $view = $root . $result[0];

        $data = $result[1] ?? [];
        extract($data);

        require_once($root . "/layout.php");
    }
}