<?php

use Dotenv\Dotenv;

class Env {

    private static $env;

    public static function init() {
        static::$env= new Dotenv(Path::root());
        static::$env->load();
    }

    public static function get(string $key) : string {
        return getenv($key);
    }
}