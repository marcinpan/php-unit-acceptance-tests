<?php

class Request
{
    private $parts;

    public function __construct(){
        $this->parts  = explode('/', $_SERVER['REQUEST_URI']);
    }

    public function page() : string {
        return $this->parts[1];
    }

    public function parameters() : array {
        return array_slice($this->parts, 2);
    }

}