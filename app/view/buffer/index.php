<title>Buffer</title>
<?php

    $pop = $_SESSION['pop'] ?? false;
    $push = $_SESSION['push'] ?? false;

    if( isset( $_SESSION['buffer'] ) ) {
        $buffer = $_SESSION['buffer'];
    }
    if( $pop ) {
        $buffer->pop();
        $_SESSION['pop'] = false;
    }
    if( $push ) {
        $buffer->push($_SESSION['value']);
        $_SESSION['push'] = false;
    }

    $_SESSION['buffer'] = $buffer;
?>

<div>
    <h2>Current size: <?php echo $buffer->size();?> </h2>
    <h3>Current capacity: <?php echo $buffer->capacity();?> </h3>

    <form method="post" action="/buffer/push">
        <label for="value">Input 1</label>
        <input id="input_0" type="text" name="input_0" value="<?php echo $buffer->at(0);?>" disabled>
        <br>
        <label for="input_1">Input 2</label>
        <input id="input_1" type="text" name="input_1" value="<?php echo $buffer->at(1);?>" disabled>
        <br>
        <label for="input_2">Input 3</label>
        <input id="input_2" type="text" name="input_2" value="<?php echo $buffer->at(2);?>" disabled>
        <br>
        <label for="value">Value</label>
        <input name="value" type="text">
        <br>
        <input type="submit" value="Push">
    </form>
    <form method="post" action="/buffer/pop">
        <input type="submit" value="Pop">
    </form>


</div>
