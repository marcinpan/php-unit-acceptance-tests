<title>Home Page</title>
<?php

$token = $_SESSION['token'] ?? null;
if($token === 'valid')
{
    ?><h2><li class="info">Email successfully confirmed!</li></h2><?php
    $_SESSION['token'] = null;
}
elseif($token === 'invalid')
{
    ?><h2><li class="error">Provided token is invalid!</li></h2><?php
    $_SESSION['token'] = null;
}
else
{
    ?><h2>Welcome to Codeception exercise!</h2><?php
}
?>
