<title>New user</title>

<div>
    <h1>New user</h1>
    <form method="post" action="/users/create">
        <label for="name">Name</label>
        <input type="text" name="name">
        <br>
        <label for="surname">Surname</label>
        <input type="text" name="surname">
        <br>
        <label for="email">Email</label>
        <input type="text" name="email">
        <br>
        <label for="password">Password</label>
        <input type="text" name="password">
        <br>
        <label for="password_confirmation">Confirm Password</label>
        <input type="text" name="password_confirmation">
        <br>
        <input name="Button" type="submit" value="Create">
    </form>
</div>
<?php
$errorArray = $errorArray ?? [];
if(!empty($errorArray))
{
    foreach ($errorArray as $error)
    {
        ?><li class="error"><?php echo $error; ?></li><?php
    }
}
$correctPassword = $correctPassword ?? true;
if(!$correctPassword)
{
    ?><li class="error">The password confirmation filed does not match the password field</li><?php
}
?>