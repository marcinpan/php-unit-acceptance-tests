<title>Users</title>
<div>
    <?php
        $users = $users ?? false;
        if(!$users){
            ?><p class="error">No users found!</p><?php
        }
        else {?>
    <ol>
        <?php foreach ($users as $user) { ?>
            <li>
                <strong><?php echo $user->name; ?></strong> - <?php echo $user->surname; ?>
            </li><?php
            }
        } ?>
    </ol>
    <a href="/users/create">New user</a>
</div>