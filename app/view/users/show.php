<title><?php echo $user->name . ' ' . $user->surname; ?></title>
<div>
    <?php
    if($user->confirmed){
        ?><h2>Welcome <?php echo $user->name . ' ' . $user->surname; ?>!</h2><?php
    }
    else{
        ?><p class="error">Please confirm your email by clicking link sent to <?php echo $user->email;?></p><?php
    }
    ?>
</div>