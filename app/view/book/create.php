<title>Book</title>
<div>
    <h1>New book</h1>
    <form method="post" action="/books/store">
        <label for="title">Title</label>
        <input type="text" name="title">
        <br>
        <label for="description">Description</label>
        <input type="text" name="description">
        <br>
        <input type="submit" value="Add">
    </form>
</div>