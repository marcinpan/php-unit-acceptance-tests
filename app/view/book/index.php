<title>Book</title>
<div>
    <ol>
        <?php foreach ($books as $book) { ?>
            <li>
                <strong><?php echo $book->title; ?></strong> - <?php echo $book->description; ?>
            </li>
        <?php } ?>
    </ol>
    <a href="/books/create">New book</a>
</div>